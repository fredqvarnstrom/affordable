<!DOCTYPE HTML>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex, nofollow">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>535 Carlton, Brooklyn NY</title>
  <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
  <script src="_inc/js/gen_validatorv31.js" type="text/javascript"></script>
  <link rel="icon" type="image/png" href="/_inc/img/">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="_inc/swiper/css/swiper.min.css" />
	<link rel="stylesheet" href="_inc/focuspoint/focuspoint.css" />
	<link rel="stylesheet" href="_inc/magnific-popup.css" />
	<link rel="stylesheet" href="_inc/style.css" />
	<script src='https://www.google.com/recaptcha/api.js'></script>
	<script type="text/javascript" src="//fast.fonts.net/jsapi/fd1d3271-cb84-494c-9e63-89fe09910889.js"></script>
</head>

<body class="<?php echo basename($_SERVER["SCRIPT_FILENAME"], '.php') ?>">

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-80712138-1', 'auto');
  ga('send', 'pageview');

</script>

  <header>
    <div id="logo">
      <a href="/" class="block">
        <img src="_inc/img/535-Carlton-Logo.svg" 
        onerror="this.src='_inc/img/535-Carlton-Logo@2x.png'; this.onerror=null;" alt="535 Carlton, Brooklyn NY"
        width="234" height="117" />
      </a>
    </div>
    <a href="" target="_blank" id="header-residents" onClick="ga('send', 'event', 'Lead', 'Residents - Header');">Residents</a>
    <div id="info_bar">
      <div class="right">
<!--
        <a href="tel:17182468080,224" onClick="ga('send', 'event', 'Contact', 'Phone');">Call</a>
        <a href="#email-us" class="mp-inline mobileHide" onClick="ga('send', 'event', 'Contact', 'Opened Form');">Email</a>
        <a href="mailto:461dean@mutualhousingny.org" class="mobileShow">Email</a>
        <a href="https://a806-housingconnect.nyc.gov/nyclottery/lottery.html#current-projects" target="_blank" id="header-apply" onClick="ga('send', 'event', 'Lead', 'Apply - Header');">Apply</a>
        <a href="" target="_blank" id="header-residents" onClick="ga('send', 'event', 'Lead', 'Residents - Header');">Residents</a>
-->
        <a href="#do-you-qualify" class="mp-inline underline" onClick="ga('send', 'pageview', '/do-you-qualify', 'Do You Qualify');">Do You Qualify?</a>
        <a href="./details" id="header-details" class="underline" onClick="ga('send', 'event', 'Lead', 'Details - Header');">Details</a>
      </div>
    </div>
  </header>
  
  <div id="do-you-qualify" class="white-popup mfp-hide">
    <!-- Steps -->
    <div id="dyq-steps" class="w100 clearfix">
      <!-- Step 1 - Household Size -->
      <div id="dyq-step-1" rel="Household Size" class="dyq-step tac">
        <h2>Household size?</h2>
        <p>
          Please count all individuals who will be living with you at 535 Carlton, including current roommates, children, parents, siblings, partner, or spouse.
        </p>
        <br />
        <select name="Household Size" id="dyq-household-size">
          <option value="0">Select Household Size</option>
          <option value="1">1 person</option>
          <option value="2">2 people</option>
          <option value="3">3 people</option>
          <option value="4">4 people</option>
          <option value="5">5 people</option>
          <option value="6">6 people</option>
          <option value="7">7+ people</option>
        </select>
      </div>
      <!-- Step 2 - Household Income -->
      <div id="dyq-step-2" rel="Household Income" class="dyq-step tac dnone">
        <h2>Household income?</h2>
        <p>
          Please total income from all sources of all household members over the age of 18. This includes wages, bonuses, tips, unemployment compensation, alimony, child support, pension benefits, Social Security benefits, recurring gifts, and public assistance.
        </p>
        <br />
        <label for="dyq-household-income" class="left">$</label>
        <input id="dyq-household-income" class="left" type="text" placeholder="Combined Annual Income">
      </div>
      <!-- Step 3 - Do you own residential real estate within 100 miles of New York City? -->
      <div id="dyq-step-3" rel="Do you own residential real estate within 100 miles of New York City?" class="dyq-step tac dnone">
        <h2>Current Property?</h2>
        <p>
          Do you own residential real estate within 100 miles of New York City?
        </p>
        <br />
        <button id="ownYes">Yes</button>
        <button id="ownNo">No</button>
      </div>
      <!-- Step 4 - Output results and display form -->
      <div id="dyq-step-4" rel="Do You Qualify? Results" class="dyq-step tac dnone">
        <!-- If they do qualify, display results and form -->
        <div id="does-qualify" class="dnone">
          <h2>
            Congratulations!<br />
            You're preliminarily eligible for:<br />
          </h2>
          <div id="dq-results"></div>
          <br />
          <p>
            To learn more, please fill out the form below.<br />Our leasing representative will contact you to schedule a time.<br />Please bring government-issued ID to your appointment.
          </p>
          <div class="fontSmall italic">
            Subject to income qualification by HDC<br />
            Household composition subject to HDC rules for unit sizes
          </div>
          <div class="w100 clearfix pt15">
            <div id="forms">
              <div class="w100 clearfix">
                <ul id="git-icons" class="pt15 pb15">
                  <li>
                    <div id="icon-email" rel="form-email" class="git-trigger current" onclick="ga('send', 'pageview', '/do-you-qualify/email', 'DYQ - Email');"></div>
                  </li>
                  <li>
                    <div id="icon-phone" rel="form-phone" class="git-trigger" onclick="ga('send', 'pageview', '/do-you-qualify/phone', 'DYQ - Phone');"></div>
                  </li>
                  <li>
                    <div id="icon-sms" rel="form-sms" class="git-trigger" onclick="ga('send', 'pageview', '/do-you-qualify/sms', 'DYQ - SMS');"></div>
                  </li>
                  <li>
                    <div id="icon-calendar" rel="form-calendar" class="git-trigger" onclick="ga('send', 'pageview', '/do-you-qualify/calendar', 'DYQ - Calendar');"></div>
                  </li>
                </ul>
              </div>
              <div id="git-forms" class="w100 clearfix pt15 pb15">
                <div id="form-email" class="git-form active w100 clearfix">
                  <form id="dyq-form" name="eform" action="_inc/php/dyq-form.php" method="post">
                    <input type="text" name="name" placeholder="Full Name*" />
                    <input type="email" name="email" placeholder="Email Address*" />
                    <input type="tel" name="phone" placeholder="Phone Number" />
                    <br />
                    <input id="send-message" type="submit" name="submit" value="Send Message">
                  </form>
                  <script language="JavaScript">
                    var frmvalidator  = new Validator("eform");
                    frmvalidator.addValidation("name","req","Please provide your name"); 
                    frmvalidator.addValidation("email","req","Please provide your email"); 
                    frmvalidator.addValidation("email","email","Please enter a valid email address"); 
                  </script>
                </div>
                <div id="form-phone" class="git-form w100 clearfix pt25 pb25">
                  <a href="tel:17182468080,224" class="button">
                    718.246.8080 x224
                  </a>
                </div>
                <div id="form-sms" class="git-form w100 clearfix"></div>
                <div id="form-calendar" class="git-form w100 clearfix"></div>
              </div>
            </div>
          </div>
        </div>
        <!-- If they do NOT qualify, let them down easy -->
        <div id="does-not-qualify">
          <h2>Sorry you do not qualify</h2>
          <p>
            Thank you for your interest in 535 Carlton,<br />unfortunately based on this preliminary quiz you are not eligible.<br />
            Should you have questions or believe there was an error, please fill out the below or call.
          </p>
          <br />
          <form id="dnq-form" name="eform" action="_inc/php/dnq-form.php" method="post">
            <input type="text" name="name" placeholder="Full Name*" />
            <input type="email" name="email" placeholder="Email Address*" />
            <input type="tel" name="phone" placeholder="Phone Number" />
            <textarea name="message" placeholder="Message" rows="5"></textarea>
            <br />
            <input id="send-message" type="submit" name="submit" value="Send Message">
          </form>
          <script language="JavaScript">
            var frmvalidator  = new Validator("eform");
            frmvalidator.addValidation("name","req","Please provide your name"); 
            frmvalidator.addValidation("email","req","Please provide your email"); 
            frmvalidator.addValidation("email","email","Please enter a valid email address"); 
          </script>
        </div>
      </div>
    </div>
  </div>