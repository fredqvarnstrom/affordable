  <footer class="w100 clearfix">
    <div class="w100 tac clearfix">
      <a href="#do-you-qualify" class="button redBG mp-inline" onClick="ga('send', 'event', 'Lead', 'Apply - Footer');">Do You Qualify?</a>
    </div>
    <div class="w100 tac clearfix">
        <div class="w33 inline-block left pt40">
          <a class="block" href="http://pacificparkbrooklyn.com" target="_blank">
            <img src="_inc/img/PPLogo.svg" alt="PPBrooklynLogo" width="220" />
          </a>
        </div>
        <div class="w33 inline-block left">
          <div class="pt45 pb25">
            <address>
              535 Carlton Ave,<br />
              Brooklyn, NY
            </address>
            <br />
            <a href="tel:17182468080,224">718.246.8080 x224</a> &nbsp; &nbsp; <a href="mailto:535Carlton@mutualhousingny.org" class="underline">535Carlton@mutualhousingny.org</a>
          </div>          
        </div>
        <div class="w33 inline-block left pt20">
          <a class="block" href="http://pacificparkbrooklyn.com/about/team" target="_blank">
            <img src="_inc/img/GFCPLogov2.jpg" alt="" width="220" height="" />
          </a>
          <img src="_inc/img/FooterLogos_02.png" alt="" width="220" height="" />
        </div>
    </div>
    <div class="w100 clearfix greyBG pt10 pb10">
      <div class="contain">
        <div class="inline-block left tal">
          <a href="http://www.silvercreativegroup.com" target="_blank" class="block">
            <div class="scgLogo">
              <span>silver</span>creative<span>.</span>
            </div>
          </a>
        </div>
        <div class="inline-block right tar">
          <span class="inline-block left">&copy; <?php echo date('Y'); ?></span>
        </div>
      </div>
    </div>
  </footer>
  <script type="text/javascript" src="_inc/js/map.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDq0snKbUQ6hm_KKameH34L2Z1pq5-cRgY&callback=initMap" async defer></script>
  <script type="text/javascript" src="_inc/js/site.js"></script>
  <script type="text/javascript" src="_inc/js/jquery.magnific-popup.min.js"></script>
  <script type="text/javascript" src="_inc/js/flowtype.js"></script>
  <script type="text/javascript" src="_inc/js/jquery.accordion.js"></script>
  <script type="text/javascript" src="_inc/focuspoint/jquery.focuspoint.min.js"></script>
  <script type="text/javascript" src="_inc/swiper/js/swiper.jquery.min.js"></script>
  <script type="text/javascript" src="_inc/js/velocity.min.js"></script>
  <script type="text/javascript" src="_inc/js/jquery.waypoints.min.js"></script>
  <script type="text/javascript" src="_inc/js/jquery.headroom.min.js"></script>
  <script src="http://a.vimeocdn.com/js/froogaloop2.min.js"></script>
</body>

</html>