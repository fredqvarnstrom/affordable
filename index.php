<?php include('header.php'); ?>

  <div id="opener" class="clearfix">
    <div>
      <h2 class="shadow whiteT">
        <span>
          NEW RENT STABILIZED<br />APARTMENTS 
        </span>
      </h2>
      <h4 class="shadow whiteT pb20">IN THE HEART OF BROWNSTONE BROOKLYN</h4>
      <br />
      <a href="#do-you-qualify" class="button mp-inline" onClick="ga('send', 'pageview', '/do-you-qualify', 'Do You Qualify');">Do You Qualify?</a>
    </div>
  </div>

  <div id="block-features" class="side-by-side clearfix">
    <div class="w100 clearfix">
      <div class="w50 h100 relative inline-block left orangeBG textTP arrow-right">
        <section>
          <h2>A Sustainable, Serene Setting</h2>
          <p>
            The residences at 535 Carlton combine unrivaled design and a premiere location in the heart of Brooklyn’s best neighborhood. With luxury fixtures and finishes, no details have gone overlooked in these studio, one, two, and three-bedroom homes. The renowned architects at COOKFOX have incorporated the serenity of the adjacent Pacific Park into the building’s fabric, creating residences that are the ideal place to call home.
          </p>
        </section>
        <img src="_inc/img/535Pattern.png" class="bg-pattern" />
      </div>
      <div class="w50 h100 relative inline-block left ohidden focuspoint" 
      data-focus-x="-0.14" data-focus-y="-0.38" data-focus-w="600" data-focus-h="467">
        <a href="_inc/img/img-pp-535carlton-bldg-ext-5556-Edit-v1.jpg" class="mp block">
          <img src="_inc/img/img-pp-535carlton-bldg-ext-5556-Edit-v1.jpg" alt="Living Room" />
        </a>
      </div>
    </div>
    <div class="w100 clearfix">
      <div class="w50 h100 relative inline-block left ohidden focuspoint" 
        data-focus-x="-0.14" data-focus-y="-0.38" data-focus-w="600" data-focus-h="467">
        <a href="_inc/img/img-pp-535carlton-int-living-5344-HDR-Edit-v1.jpg" class="mp block">
          <img src="_inc/img/img-pp-535carlton-int-living-5344-HDR-Edit-v1.jpg" alt="Living Room" />
        </a>
      </div>
      <div class="w50 h100 relative inline-block left orangeBG textTP arrow-right">
        <section>
          <h2>A Luxurious Living Experience</h2>
          <p>Create a comfortable, inviting home filled with natural light flowing in through the large windows of every 535 Carlton residence. Enjoy views of Manhattan, Pacific Park and tree-lined brownstone Brooklyn along Dean and Carlton. Bring family and friends together in a space designed to be lived in, in a luxury building that always considers its residents and their loved ones.</p>
        </section>
        <img src="_inc/img/535Pattern.png" class="bg-pattern" />
      </div>
    </div>
    <div id="secondFeature" class="w100 clearfix">
      <div class="w50 h100 relative inline-block left orangeBG textTP arrow-left">
        <section>
          <h2>A Modern Kitchen that Inspires</h2>
          <p>
            From cooking a family dinner to a holiday feast, the open kitchens at 535 Carlton are perfect for every occasion. All units include state-of-the-art appliances, including a Whirlpool gas range, over-the-counter microwave, dishwasher and refrigerator along with custom cabinets and solid stone countertops.
          </p>
        </section>
        <img src="_inc/img/535Pattern.png" class="bg-pattern" />
      </div>
      <div class="w50 h100 relative inline-block left ohidden focuspoint" 
        data-focus-x="-0.10" data-focus-y="-0.17" data-image-w="1200" data-image-h="825">
        <a href="_inc/img/kitchen-update.jpg" class="mp block">
          <img src="_inc/img/kitchen-update.jpg" alt="Kitchen" />
        </a>
      </div>
    </div>
<!--
    <div class="w100 clearfix" data-os-animation="fadeInLeft" data-os-animation-delay="0s">
      <div class="w50 h100 relative inline-block left orangeBG textTP arrow-right">
        <section>
          <h2>The Bathroom: Refined and Reorganized</h2>
          <p>Enjoy a reprieve from the rush of the city pace in the spa-like décor of your stylish bathroom. Allow the warmth of the stone wall panels and porcelain tiles envelope you. Relish in the comfort and efficiency of a Toto in-wall toilet and polished Grohe sink and fixtures. With the addition of an Asko washer & dryer in each residence you have nearly everything you need in the comfort of your home and it becomes clear how modular is better.</p>
        </section>
        <img src="_inc/img/535Pattern.png" class="bg-pattern" />
      </div>
      <div class="w50 h100 relative inline-block left ohidden focuspoint" 
      data-focus-x="-0.02" data-focus-y="-0.31" data-focus-w="960" data-focus-h="1200">
        <a href="_inc/img/Bath.jpg" class="mp block">
          <img src="_inc/img/Bath.jpg" alt="Bathroom" />
        </a>
      </div>
    </div>
-->
  </div>
  
  <div id="grid" class="grid-images clearfix">
    <div class="w100 clearfix pb80">
      <h2 class="uppercase tac pb20 pt35">Amenities</h2>
      <div class="barredBox tac w100">
        <div class="contain-grid w16">
          <div class="w100 h100 orangeBG">
            <div class="w100 h100 table">
              <div class="w100 h100 tableCell vam tac">
                <h3 class="uppercase">Fitness Center</h3>
              </div>
            </div>
          </div>
        </div>
        <div class="contain-grid w16">
          <div class="w100 h100 orangeBG">
            <div class="w100 h100 table">
              <div class="w100 h100 tableCell vam tac">
                <h3 class="uppercase">Children's Playroom</h3>
              </div>
            </div>
          </div>
        </div>
        <div class="contain-grid w16">
          <div class="w100 h100 orangeBG">
            <div class="w100 h100 table">
              <div class="w100 h100 tableCell vam tac">
                <h3 class="uppercase">Residents' Lounge</h3>
              </div>
            </div>
          </div>
        </div>
        <div class="contain-grid w16">
          <div class="w100 h100 orangeBG">
            <div class="w100 h100 table">
              <div class="w100 h100 tableCell vam tac">
                <h3 class="uppercase">Residents' Terrace with Community Garden</h3>
              </div>
            </div>
          </div>
        </div>
        <div class="contain-grid w16">
          <div class="w100 h100 orangeBG">
            <div class="w100 h100 table">
              <div class="w100 h100 tableCell vam tac">
                <h3 class="uppercase">Bike Room</h3>
              </div>
            </div>
          </div>
        </div>
        <div class="contain-grid w16">
          <div class="w100 h100 orangeBG">
            <div class="w100 h100 table">
              <div class="w100 h100 tableCell vam tac">
                <h3 class="uppercase">Laundry on Each Floor</h3>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <div id="block-amenities" class="orangeBG">
    <div id="gallery" class="w100 ohidden">
      <!-- Slider main container -->
      <div class="swiper-container">
          <!-- Additional required wrapper -->
          <div class="swiper-wrapper">
              <!-- Slides -->
              <div class="swiper-slide focuspoint" data-focus-x="-0.00" data-focus-y="-0.93" data-image-w="1200" data-image-h="800">
                <img src="_inc/img/img-pp-535carlton-int-lobby-5519-Edit.jpg" alt="" />
              </div>
              <div class="swiper-slide focuspoint" data-focus-x="-0.03" data-focus-y="-0.51" data-focus-w="1200" data-focus-h="800">
                <img src="_inc/img/535C-render-Lobby_Back.jpg" alt="" />
              </div>
              <div class="swiper-slide focuspoint" data-focus-x="0.18" data-focus-y="-0.05" data-image-w="1200" data-image-h="800">
                <img src="_inc/img/img-pp-535carlton-bldg-ext-common-8455-Edit.jpg" alt="" />
              </div>
              <div class="swiper-slide focuspoint" data-focus-x="-0.00" data-focus-y="-0.43" data-image-w="1200" data-image-h="675">
                <img src="_inc/img/img-pp-535carlton-bldg-int-common-4989-HDR-Edit.jpg" alt="" />
              </div>
              <div class="swiper-slide focuspoint" data-focus-x="0.02" data-focus-y="-0.42" data-image-w="1200" data-image-h="800">
                <img src="_inc/img/img-pp-535carlton-bldg-int-common-5054-Edit.jpg" alt="" />
              </div>
              <div class="swiper-slide focuspoint" data-focus-x="0.00" data-focus-y="-0.26" data-image-w="1200" data-image-h="800">
                <img src="_inc/img/Gym@2x.png" alt="" />
              </div>
              <div class="swiper-slide focuspoint" data-focus-x="0.00" data-focus-y="-0.62" data-image-w="1200" data-image-h="800">
                <img src="_inc/img/img-pp-535carlton-int-bedrm-5249-Edit.jpg" alt="" />
              </div>
              <div class="swiper-slide focuspoint" data-focus-x="-0.02" data-focus-y="-0.62" data-image-w="1200" data-image-h="800">
                <img src="_inc/img/img-pp-535carlton-int-bedrm-5379-HDR-Edit.jpg" alt="" />
              </div>
              <div class="swiper-slide focuspoint" data-focus-x="0.00" data-focus-y="-0.40" data-image-w="1200" data-image-h="800">
                <img src="_inc/img/img-pp-535carlton-int-kitchen-5339-Edit.jpg" alt="" />
              </div>
              <div class="swiper-slide focuspoint" data-focus-x="0.00" data-focus-y="-0.40" data-image-w="1200" data-image-h="800">
                <img src="_inc/img/img-pp-535carlton-int-living-5394-Edit.jpg" alt="" />
              </div>
          </div>
          <!-- If we need pagination -->
          <div class="swiper-pagination"></div>
          
          <!-- If we need navigation buttons -->
          <div class="swiper-button-prev"></div>
          <div class="swiper-button-next"></div>
          
          <!-- If we need scrollbar -->
          <div class="swiper-scrollbar"></div>
      </div>
    </div>
    <div class="tac orangeBG">
      <div class="contain">
      <div id="header-amenities" class="pt30 pb30 zDex">
        <h3>
          About The Process
        </h3>
      </div>
      <div id="content-amenities" class="orangeBG pl40 pr40 pb75 textT">
        <div class="barredBox tac">
          <section>
            <p>
<a href="#do-you-qualify" class="mp-inline underline" onClick="ga('send', 'event', 'Do You Qualify', 'Opened Form');">Complete this form</a> to see if you may be eligible for a brand new apartment at 535 Carlton.  You will then have the opportunity to tour the building and available floor plans before continuing the application process. For details about necessary documentation for the application process, visit our <a href="./details" class="underline">Details</a> page. 
            </p>
          </section>
        </div>
<!--
        <div class="w100 clearfix pt25">
          <div class="w100 left inline-block pr20">
            <div id="amenities">
              <section>
                <h3>Amenities</h3>
                <br />
                <ul class="w50 left inline-block pr20">
                  <li>
                    <h4>&bull; State-of-the-art fitness equipment & yoga/dance studio</h4>
                  </li>
                  <li>
                    <h4>&bull; Resident Lounge with WiFi</h4> 
                  </li>
                  <li>
                    <h4>&bull; Game Room with Billiards and video games</h4>
                  </li>
                </ul>
                <ul class="w50 left inline-block pl20">
                  <li>
                    <h4>&bull; Children's Play room with soft flooring and changing station</h4>
                  </li>
                  <li>
                    <h4>&bull; Art Studio for creative inspiration</h4>
                  </li>
                  <li>
                    <h4>&bull; Rooftop Lounge and Terrance with catering kitchen</h4>
                  </li>
                </ul>
              </section>
            </div>
          </div>
        </div>
-->
      
      </div>
      </div>
      <img src="_inc/img/535Pattern.png" class="bg-pattern" />
    </div>
  </div>
  
  <div id="block-video" class="orangeBG clearfix">
    <div id="videoBox">
      <div id="vimeoPreview" class="absolute w100 h100">
        <div class="tableCell vam tac">
          <img src="_inc/img/PP-White.svg" alt="Pacific Park" class="block relative zDex m0a" width="440" height="142" /><br />
          <br />
          <a onclick="" id="playMovie" class="button">Watch Video</a>
        </div>
      </div>
      <iframe id="vimeovid" src="https://player.vimeo.com/video/147793791?title=0&byline=0&portrait=0&autoplay=0" width="100%" height="auto" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
    </div>
    <div class="contain pt45 pb45 textT">
      <h2 class="uppercase tac zDex">This is Brooklyn</h2>
      <h4 class="uppercase whiteT pb25 tac zDex">Art. Music. Entrepreneurial Drive. Neighborhood Charm.</h4>
      <div class="barredBox tac">
        <section>
          <p>
            Brooklyn, New York’s creative nexus, is known for its art, music and innovative entrepreneurs. It is also known for its tree-lined streets, world renowned restaurants and shopping. At Pacific Park Brooklyn, residents live in an urban oasis with a leafy eight-acre park, while having access to everything that makes New York the greatest city in the world. 
          </p>
          <br />
          <p>
            Greenland Forest City Partners, in conjunction with world-class architects COOKFOX, SHoP, Marvel and KPF, are breathing life into this thriving community designed for all New Yorkers. At Pacific Park Brooklyn, there’s no need to choose between a day at the park and a day around town. Everything that makes the city, and coming home, great is right here.
          </p>
          <br />
          <p>
            535 Carlton is located in School District 13
          </p>
        </section>
      </div>      
    </div>
    <img src="_inc/img/535Pattern.png" class="bg-pattern" />
  </div>
  
  <div id="map-area" class="orangeBG">
    <div id="map" class="zDex"></div>
    <div class="contain">
      <section>
        <div id="map-legend">
          <div class="inline-block left relative">
            <img src="_inc/img/Icon-Restaurant@2x.png" alt="" width="" height="" alt="Restaurants" />
            Restaurant
          </div>
          <div class="inline-block left relative">
            <img src="_inc/img/Icon-Bar@2x.png" alt="" width="" height="" alt="Bar" />
            Bar
          </div>
          <div class="inline-block left relative">
            <img src="_inc/img/Icon-Cafe@2x.png" alt="" width="" height="" alt="Cafe" />
            Cafe
          </div>
          <div class="inline-block left relative">
            <img src="_inc/img/Icon-LocalAttraction@2x.png" alt="" width="" height="" alt="Local Attraction" />
            Local Attraction
          </div>
          <div class="inline-block left relative">
            <img src="_inc/img/Icon-School@2x.png" alt="" width="" height="" alt="School" />
            Education
          </div>
          <div class="inline-block left relative">
            <img src="_inc/img/Icon-Groceries@2x.png" alt="" width="" height="" alt="Groceries" />
            Groceries            
          </div>
        </div>
<!--
        <div id="header-area" class="tac pb30 pt30">
          <h3>
            Vibrant Brooklyn
          </h3>
        </div>
        <div id="content-area" class="pb40 textT">
          <div class="barredBox tac">
            <p>
535 Carlton is the second affordable housing rental residential building to open at Pacific Park Brooklyn, the borough’s newest neighborhood extending from 4th Avenue to Vanderbilt Avenue in the heart of the borough. Pacific Park sits at the crossroads of Brooklyn’s most important transportation hub, blooming green space and top shopping and restaurants. Pacific Park Brooklyn: welcome home.
            </p>
          </div>
        </div>  
--> 
      </section>
    </div>
    <img src="_inc/img/535Pattern.png" class="bg-pattern" />
  </div>
  
  <div id="grid" class="grid-images clearfix">
    
      <div class="contain-grid">
        <div data-focus-x="0.13" data-focus-y="0.18" data-focus-w="523" data-focus-h="463">
          <img src="_inc/img/squares/Sq-1.png" alt="" />
        </div>
      </div>
      <div class="contain-grid">
        <div data-focus-x="0.10" data-focus-y="0.16" data-focus-w="523" data-focus-h="463">
          <img src="_inc/img/Subway.png" alt="" />
        </div>
      </div>
      <div class="contain-grid">
        <div data-focus-x="0.41" data-focus-y="0.42" data-focus-w="375" data-focus-h="376">
          <img src="_inc/img/squares/Sq-3.png" alt="" />
        </div>
      </div>
      <div class="contain-grid">
        <div data-focus-x="-0.30" data-focus-y="0.32" data-focus-w="376" data-focus-h="376">
          <img src="_inc/img/squares/Sq-4.png" alt="" />
        </div>
      </div>
      <div class="contain-grid">
        <div class="w100 h100 orangeBG">
          <div class="w100 h100 table">
            <div class="w100 h100 tableCell vam tac">
              <h3 class="barredBox whiteT">Brooklyn's<br />Charm</h3>
            </div>
          </div>
        </div>
      </div>
      <div class="contain-grid">
        <div data-focus-x="0.10" data-focus-y="0.16" data-focus-w="523" data-focus-h="463">
          <img src="_inc/img/squares/Sq-6.png" alt="" />
        </div>
      </div>
      <div class="contain-grid">
        <div data-focus-x="0.10" data-focus-y="0.16" data-focus-w="523" data-focus-h="463">
          <img src="_inc/img/Kids.png" alt="" />
        </div>
      </div>
      <div class="contain-grid">
        <div data-focus-x="0.10" data-focus-y="0.16" data-focus-w="523" data-focus-h="463">
          <img src="_inc/img/Bk.png" alt="" />
        </div>
      </div>
      <div class="contain-grid">
        <div data-focus-x="0.10" data-focus-y="0.16" data-focus-w="523" data-focus-h="463">
          <img src="_inc/img/Sq4-B.jpg" alt="" />
        </div>
      </div>
    </div>
  
<?php include('footer.php'); ?>