<?php

// Push data to the google sheet
function push_google_sheet($d){

    $spreadsheetId_all = '11xRd6SRCuzUuqXx6eIvZ_mVrZcRHVMAz2gG3gm8rLiE';
    $spreadsheetId_dyq = '1t_QWsGDAmMcf_jEJAE_1orWlQ7R9ma_LtYNpVic6GKc';
    $spreadsheetId_dnq = '1ekyKblf3sNEeH2UEiVxccjKhtezB7o-K-SThCh5cVtI';

    require_once __DIR__ . '/../../vendor/autoload.php';

    define('CLIENT_SECRET_PATH', __DIR__ . '/client_secret.json');
    define('SCOPES', implode(' ', array(
            Google_Service_Sheets::SPREADSHEETS)
    ));

    $client = new Google_Client;
    $client->useApplicationDefaultCredentials();
    $client->setApplicationName("Add Rows to All Applicants / Qualified Applicants");
    $client->setScopes(SCOPES);
    $client->setAuthConfig(CLIENT_SECRET_PATH);
    $client->setAccessType('offline');
    if ($client->isAccessTokenExpired()) {
        $client->refreshTokenWithAssertion();
    }

    $service = new Google_Service_Sheets($client);

    $options = array('valueInputOption' => 'USER_ENTERED');
    if ($d['type'] == 'dyq'){
        if ($d['dyq-household-size'] == null){
            $d['dyq-household-size'] = 0;
        }
        if ($d['dyq-household-income'] == null){
            $d['dyq-household-income'] = 0;
        }
        $values = [[$d['name'], $d['email'], $d['phone'], $d['dyq-household-size'], $d['dyq-household-income'], implode(',  ', $d['result'])]];
    }
    else{
        if ($d['dnq-household-size'] == null){
            $d['dnq-household-size'] = 0;
        }
        if ($d['dnq-household-income'] == null){
            $d['dnq-household-income'] = 0;
        }
        $values = [[$d['name'], $d['email'], $d['phone'], $d['dnq-household-size'], $d['dnq-household-income'], $d['message']]];
    }

    $body = new Google_Service_Sheets_ValueRange(['values' => $values]);

    if ($d['type'] == 'dyq'){
        $service->spreadsheets_values->append($spreadsheetId_dyq, 'A1:F1', $body, $options);
    }
    else{
        $service->spreadsheets_values->append($spreadsheetId_dnq, 'A1:F1', $body, $options);
    }

    $service->spreadsheets_values->append($spreadsheetId_all, 'A1:F1', $body, $options);
}