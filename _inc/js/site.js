$ = jQuery.noConflict();
$(document).ready(function() {
  
  // Do you qualify quiz
    // Possible Outcomes
    var o1  = {rent:'$2,137', beds:'Studio', people:1, minimumIncome:74606, maxiumIncome:104775};
    var o2  = {rent:'$2,680', beds:1, people:1, minimumIncome:93258, maxiumIncome:104775};
    var o3  = {rent:'$2,680', beds:1, people:2, minimumIncome:93258, maxiumIncome:119625};
    var o4  = {rent:'$3,223', beds:2, people:2, minimumIncome:111909, maxiumIncome:119625};
    var o5  = {rent:'$3,223', beds:2, people:3, minimumIncome:111909, maxiumIncome:134640};
    var o6  = {rent:'$3,223', beds:2, people:4, minimumIncome:111909, maxiumIncome:149490};
    var o7  = {rent:'$3,716', beds:3, people:3, minimumIncome:129258, maxiumIncome:134640};
    var o8  = {rent:'$3,716', beds:3, people:4, minimumIncome:129258, maxiumIncome:149490};
    var o9  = {rent:'$3,716', beds:3, people:5, minimumIncome:129258, maxiumIncome:161535};
    var o10 = {rent:'$3,716', beds:3, people:6, minimumIncome:129258, maxiumIncome:173415};
    var pOs = new Array(o1,o2,o3,o4,o5,o6,o7,o8,o9,o10);
    // Setup Variables
    var householdSize   = 0;
    var householdIncome = 0;
    var trueFalse       = 0;
    // Step 1 - Household Size
    $('#dyq-household-size').on('change', function(){
      // Update Variable for Household Size
      householdSize = $(this).val();
      // Fade Out This Step, Fade In Step 2
      $('#dyq-step-1').fadeOut(0);
      $('#dyq-step-2').fadeIn(300);
      // Send event to Google Analytics
      ga('send', 'event', 'Do You Qualify? Step 1', 'input', $(this).val());
    });
    // Step 2 - Household Income
    $('#dyq-household-income').on('change', function(){
      // Update Variable for Household Income
      householdIncome = parseInt($(this).val().replace(/[^\d\.]/g, ''));
      // Fade Out This Step, Fade In Step 3
      $('#dyq-step-2').fadeOut(0);
      $('#dyq-step-3').fadeIn(300);
      // Send event to Google Analytics
      ga('send', 'event', 'Do You Qualify? Step 2', 'amount', $(this).val());
    });
    // Step 3 - Current Property in NYC area
      // If they say YES, end it
      $('#ownYes').on('click touch', function() {
        $('#dyq-step-3').fadeOut(0);
        $('#dyq-step-4,#does-not-qualify').fadeIn(300);
        // Include in form
        $('#dnq-form').append('<input type="hidden" name="dnq-household-size" value="'+ householdSize +'" /><input type="hidden" name="dnq-household-income" value="'+ householdIncome +'" />');
        // Send event to Google Analytics
        ga('send', 'event', 'Do You Qualify? Step 3', 'choice', 'no');
      });
      // If they say NO, provide results
      $('#ownNo').on('click touch', function() {
        // Fade Out Step 3
        $('#dyq-step-3').fadeOut(0);
        $('#dyq-step-4').fadeIn(300);
        // Include in form
        $('#dyq-form').append('<input type="hidden" name="dyq-household-size" value="'+ householdSize +'" /><input type="hidden" name="dyq-household-income" value="'+ householdIncome +'" />');
        // Loop through possible outcomes
        var i = 0;
        $.each(pOs,function() {
          // If the shoe fits...
          if(householdIncome >= this.minimumIncome && householdIncome <= this.maxiumIncome && householdSize == this.people) {
            i++;
            // Print results
            $('#dq-results').append('<h4>' + this.beds + ' Bedroom at ' + this.rent + '/month<br /></h4>');
            // Include in form
            $('#dyq-form').append('<input type="hidden" name="result['+ i +']" value="'  + this.beds + ' Bedroom at ' + this.rent + '/month" />');
            // Fade In Results
            $('#does-qualify').fadeIn(300);
            $('#does-not-qualify').fadeOut(0);
            // Send event to Google Analytics
            ga('send', 'event', 'Do You Qualify? Step 3', 'choice', this);
          }
        });
        // Send event to Google Analytics
        // ga('send', 'event', 'Do You Qualify?', 'input', 'Current Property', 'yes');
      });

    var swiper = [];
    $('.swiper-container').each(function(index) {
      swiper[index] = new Swiper(this, {
        pagination: $('.swiper-pagination', this),
        nextButton: $('.swiper-button-next', this),
        prevButton: $('.swiper-button-prev', this),
        paginationClickable: true,
        loop: true,
        effect: 'coverflow',
        autoplay: 5200,
        autoplayDisableOnInteraction: true,
        lazyLoading: true,
        cube: {
          shadow: false,
          slideShadows: false,
          shadowOffset: 0,
          shadowScale: 0
        },
        onInit: function(){
          $(".focuspoint,.swiper-slide,.contain-grid").focusPoint();
        }
      });
    });
    
    $('.mp').magnificPopup({
        type: 'image',
        gallery: {
            enabled: true
        }
    });
    $('.mp-inline').magnificPopup({
        type: 'inline'
    });
    $(".focuspoint,.swiper-slide,.contain-grid").focusPoint();
    $(".textT h2,.textTP h2").flowtype({
        minFont: 25,
        maxFont: 35,
        fontRatio: 10
    });
    $(".textT h3").flowtype({
        minFont: 25,
        maxFont: 45,
        fontRatio: 9
    });
    $(".textTP p").flowtype({
        minFont: 15,
        maxFont: 18,
        fontRatio: 17
    });
    $(".textT p").flowtype({
        minFont: 17,
        maxFont: 25,
        fontRatio: 17
    });
    $(".accordion").accordion({
        "transitionSpeed": 400
    });
    var iframe = $('#vimeovid')[0];
    var player = $f(iframe);
    $('#playMovie').click(function() {
        player.api('play');
        $("#vimeoPreview").hide();
    });
/*
    $("body").velocity("fadeIn", {
        duration: 1000
    }, {
        easing: [0.68, -0.55, 0.265, 1.55]
    });
*/
/*
    $('.orangeBG p').bind('inview', function(event, visible) {
        if (visible == true) {
            $(this).velocity("fadeIn", {
                duration: 1000,
                delay: 200
            });
        } else {
            $(this).velocity("reverse");
        }
    });
    $('.orangeBG h3:not(".barredBox")').bind('inview', function(event, visible) {
        if (visible == true) {
            $(this).velocity("fadeIn", {
                duration: 1000,
                delay: 200
            });
        } else {
            $(this).velocity("reverse");
        }
    });
    $('#vimeoPreview .tableCell').bind('inview', function(event, visible) {
        if (visible == true) {
            $("#playMovie").velocity("fadeIn", {
                duration: 1000,
                delay: 1000
            });
        } else {
            $("#playMovie").velocity("reverse");
        }
    });
    $('.contain-grid').bind('inview', function(event, visible) {
        if (visible == true) {
            $(this).velocity("fadeIn", {
                duration: 1000,
                delay: 200
            });
        } else {
            $(this).velocity("reverse");
        }
    });
*/
    if ($(window).width() < 650) {
        $('#secondFeature .focuspoint').insertBefore('#secondFeature .textTP');
    }
    // Headroom - Stick Header to Top When Scrolling
    $('header').headroom({
      tolerance: {
        up: 15,
        down: 30
      }
    });
    
    // Get In Touch - Switch
    $('#git-forms div.active').show();
    $('#git-icons li').on('click touch', 'div', function(){
      gitTarget = $(this).attr('rel');
      $('#'+gitTarget).addClass('active').siblings().removeClass('active');
      $(this).addClass('current');
      $('#git-listing #git-icons .git-trigger[rel="'+gitTarget+'"]').addClass('current');
      $('#git-icons .git-trigger:not([rel="'+gitTarget+'"])').removeClass('current');
    });
    
});