var map;
var infowindow;
window.initMap = function() {
    var pacificPark = {
        lat: 40.683845,
        lng: -73.975433
    };
    var isDraggable = $(document).width() > 480 ? true : false;
    map = new google.maps.Map(document.getElementById('map'), {
        center: pacificPark,
        overviewMapControl: false,
        mapTypeControl: false,
        disableDefaultUI: true,
        zoomControl: true,
        scrollwheel: false,
        draggable: isDraggable,
        zoom: 15,
        styles: [{
            "featureType": "landscape",
            "elementType": "geometry",
            "stylers": [{
                "visibility": "simplified"
            }, {
                "color": "#f5f5f5"
            }]
        }, {
            "featureType": "poi.park",
            "elementType": "geometry",
            "stylers": [{
                "visibility": "off"
            }]
        }, {
            "featureType": "poi",
            "stylers": [{
                "visibility": "off"
            }]
        }, {
            "featureType": "water",
            "elementType": "geometry",
            "stylers": [{
                "visibility": "simplified"
            }, {
                "color": "#BCDDFB"
            }]
        }, {
            "featureType": "road.highway",
            "elementType": "geometry.fill",
            "stylers": [{
                "visibility": "on"
            }, {
                "color": "#ff8080"
            }]
        }, {
            "featureType": "transit.station",
            "elementType": "labels.text",
            "stylers": [{
                "visibility": "off"
            }]
        }, ]
    });
    var transitLayer = new google.maps.TransitLayer();
    transitLayer.setMap(map);
    var polyCoords1 = [{
        lng: -73.971055,
        lat: 40.680878
    }, {
        lng: -73.970588,
        lat: 40.682103
    }, {
        lng: -73.970566,
        lat: 40.682261
    }, {
        lng: -73.967509,
        lat: 40.681625
    }, {
        lng: -73.968002,
        lat: 40.680237
    }, {
        lng: -73.971052,
        lat: 40.680878
    }];
    var poly1 = new google.maps.Polygon({
        paths: polyCoords1,
        strokeColor: '#868733',
        strokeOpacity: 0.8,
        strokeWeight: 0,
        fillColor: '#868733',
        fillOpacity: 0.7
    });
    poly1.setMap(map);
    var polyCoords2 = [{
        lng: -73.973866,
        lat: 40.682306
    }, {
        lng: -73.973651,
        lat: 40.682900
    }, {
        lng: -73.970690,
        lat: 40.682265
    }, {
        lng: -73.970701,
        lat: 40.682168
    }, {
        lng: -73.970894,
        lat: 40.681663
    }, {
        lng: -73.973855,
        lat: 40.682306
    }];
    var poly2 = new google.maps.Polygon({
        paths: polyCoords2,
        strokeColor: '#868733',
        strokeOpacity: 0.8,
        strokeWeight: 0,
        fillColor: '#868733',
        fillOpacity: 0.7
    });
    poly2.setMap(map);
    var polyCoords3 = [{
        lng: -73.973908,
        lat: 40.682212
    }, {
        lng: -73.974155,
        lat: 40.681529
    }, {
        lng: -73.973670,
        lat: 40.681423
    }, {
        lng: -73.973391,
        lat: 40.682113
    }, {
        lng: -73.973903,
        lat: 40.682216
    }];
    var poly3 = new google.maps.Polygon({
        paths: polyCoords3,
        strokeColor: '#868733',
        strokeOpacity: 0.8,
        strokeWeight: 0,
        fillColor: '#868733',
        fillOpacity: 0.7
    });
    poly3.setMap(map);
    var polyCoords4 = [{
        lng: -73.974284,
        lat: 40.681574
    }, {
        lng: -73.973790,
        lat: 40.682930
    }, {
        lng: -73.975990,
        lat: 40.683388
    }, {
        lng: -73.976140,
        lat: 40.683486
    }, {
        lng: -73.977234,
        lat: 40.683897
    }, {
        lng: -73.975775,
        lat: 40.681875
    }, {
        lng: -73.974284,
        lat: 40.681574
    }];
    var poly4 = new google.maps.Polygon({
        paths: polyCoords4,
        strokeColor: '#868733',
        strokeOpacity: 0.8,
        strokeWeight: 0,
        fillColor: '#868733',
        fillOpacity: 0.7
    });
    poly4.setMap(map);
    var polyCoords5 = [{
        lng: -73.977433,
        lat: 40.683303
    }, {
        lng: -73.977208,
        lat: 40.683624
    }, {
        lng: -73.977486,
        lat: 40.684010
    }, {
        lng: -73.978243,
        lat: 40.684295
    }, {
        lng: -73.978594,
        lat: 40.683736
    }, {
        lng: -73.977433,
        lat: 40.683299
    }];
    var poly5 = new google.maps.Polygon({
        paths: polyCoords5,
        strokeColor: '#868733',
        strokeOpacity: 0.8,
        strokeWeight: 0,
        fillColor: '#868733',
        fillOpacity: 0.7
    });
    poly5.setMap(map);
    var json = [{
        "id": 1,
        "description": "<div class='infobox'>535 Carlton</div>",
        "latitude": "40.681515",
        "longitude": "-73.970852",
        "icon": "_inc/img/535-Pin@2x.png"
    }, {
        "id": 2,
        "description": "<div class='infobox'>BAM</div>",
        "latitude": "40.687136",
        "longitude": "-73.977034",
        "icon": "_inc/img/Icon-LocalAttraction@2x.png"
    }, {
        "id": 3,
        "description": "<div class='infobox'>Sopebox Gallery</div>",
        "latitude": "40.678178",
        "longitude": "-73.944158",
        "icon": "_inc/img/Icon-LocalAttraction@2x.png"
    }, {
        "id": 4,
        "description": "<div class='infobox'>Patsy’s Pizzeria</div>",
        "latitude": "40.6818278",
        "longitude": "-73.976196",
        "icon": "_inc/img/Icon-Restaurant@2x.png"
    }, {
        "id": 5,
        "description": "<div class='infobox'>Alchemy Restaurant & Tavern</div>",
        "latitude": "40.6813424",
        "longitude": "-73.9772824",
        "icon": "_inc/img/Icon-Bar@2x.png"
    }, {
        "id": 6,
        "description": "<div class='infobox'>Convivium Osteria</div>",
        "latitude": "40.6810501",
        "longitude": "-73.9774943",
        "icon": "_inc/img/Icon-Restaurant@2x.png"
    }, {
        "id": 7,
        "description": "<div class='infobox'>Blue Sky Bakery Inc</div>",
        "latitude": "40.68114",
        "longitude": "-73.977051",
        "icon": "_inc/img/Icon-Cafe@2x.png"
    }, {
        "id": 8,
        "description": "<div class='infobox'>Peperoncino</div>",
        "latitude": "40.6808304",
        "longitude": "-73.9776871",
        "icon": "_inc/img/Icon-Restaurant@2x.png"
    }, {
        "id": 10,
        "description": "<div class='infobox'>Bergen Bagels</div>",
        "latitude": "40.6809384",
        "longitude": "-73.974769",
        "icon": "_inc/img/Icon-Cafe@2x.png"
    }, {
        "id": 11,
        "description": "<div class='infobox'>Bklyn Larder</div>",
        "latitude": "40.6805496",
        "longitude": "-40.6805496",
        "icon": "_inc/img/Icon-Restaurant@2x.png"
    }, {
        "id": 12,
        "description": "<div class='infobox'>Sugarcane</div>",
        "latitude": "40.6803229",
        "longitude": "-73.9749531",
        "icon": "_inc/img/Icon-Cafe@2x.png"
    }, {
        "id": 13,
        "description": "<div class='infobox'>Woodland</div>",
        "latitude": "40.6801991",
        "longitude": "-73.9749188",
        "icon": "_inc/img/Icon-Bar@2x.png"
    }, {
        "id": 14,
        "description": "<div class='infobox'>Cubana Café</div>",
        "latitude": "40.6826519",
        "longitude": "-73.9935226",
        "icon": "_inc/img/Icon-Restaurant@2x.png"
    }, {
        "id": 15,
        "description": "<div class='infobox'>Flatbush Farm</div>",
        "latitude": "40.6797904",
        "longitude": "-73.9747283",
        "icon": "_inc/img/Icon-Restaurant@2x.png"
    }, {
        "id": 16,
        "description": "<div class='infobox'>Pathmark</div>",
        "latitude": "40.683964",
        "longitude": "-73.9754677",
        "icon": "_inc/img/Icon-Groceries@2x.png"
    }, {
        "id": 17,
        "description": "<div class='infobox'>Ganso Yaki</div>",
        "latitude": "40.6854892",
        "longitude": "-73.9805335",
        "icon": "_inc/img/Icon-Restaurant@2x.png"
    }, {
        "id": 18,
        "description": "<div class='infobox'>Museum of Contemporary African Diasporan Arts</div>",
        "latitude": "40.6852146",
        "longitude": "-73.9743774",
        "icon": "_inc/img/Icon-LocalAttraction@2x.png"
    }, {
        "id": 19,
        "description": "<div class='infobox'>Chuko Ramen</div>",
        "latitude": "40.680053",
        "longitude": "-73.968125",
        "icon": "_inc/img/Icon-Restaurant@2x.png"
    }, {
        "id": 20,
        "description": "<div class='infobox'>Key Food</div>",
        "latitude": "40.679446",
        "longitude": "-73.978937",
        "icon": "_inc/img/Icon-Groceries@2x.png"
    }, {
        "id": 21,
        "description": "<div class='infobox'>Public School 38</div>",
        "latitude": "40.685034",
        "longitude": "-73.982911",
        "icon": "_inc/img/Icon-School@2x.png"
    }, {
        "id": 22,
        "description": "<div class='infobox'>Brooklyn Free School</div>",
        "latitude": "40.687086",
        "longitude": "-73.968016",
        "icon": "_inc/img/Icon-School@2x.png"
    }, {
        "id": 23,
        "description": "<div class='infobox'>Acorn Community High School</div>",
        "latitude": "40.678585",
        "longitude": "-73.961943",
        "icon": "_inc/img/Icon-School@2x.png"
    }, {
        "id": 24,
        "description": "<div class='infobox'>Brooklyn Tech High School</div>",
        "latitude": "40.689573",
        "longitude": "-73.976253",
        "icon": "_inc/img/Icon-School@2x.png"
    }, {
        "id": 25,
        "description": "<div class='infobox'>Paul Robeson Freedom School</div>",
        "latitude": "40.682652",
        "longitude": "-73.967264",
        "icon": "_inc/img/Icon-School@2x.png"
    }, {
        "id": 26,
        "description": "<div class='infobox'>Achievement First Endeavor Elementary</div>",
        "latitude": "40.682396",
        "longitude": "-73.965924",
        "icon": "_inc/img/Icon-School@2x.png"
    }, {
        "id": 27,
        "description": "<div class='infobox'>PS 11 Purvis Elementary</div>",
        "latitude": "40.685367",
        "longitude": "-73.966101",
        "icon": "_inc/img/Icon-School@2x.png"
    }, {
        "id": 28,
        "description": "<div class='infobox'>Math and Science Exploratory</div>",
        "latitude": "40.683928",
        "longitude": "-73.980294",
        "icon": "_inc/img/Icon-School@2x.png"
    }, {
        "id": 29,
        "description": "<div class='infobox'>Lango NYC</div>",
        "latitude": "40.683474",
        "longitude": "-73.981809",
        "icon": "_inc/img/Icon-School@2x.png"
    }, {
        "id": 30,
        "description": "<div class='infobox'>Brooklyn High School of the Arts</div>",
        "latitude": "40.683835",
        "longitude": "-73.980355",
        "icon": "_inc/img/Icon-School@2x.png"
    }, {
        "id": 31,
        "description": "<div class='infobox'>Brooklyn Music School</div>",
        "latitude": "40.686152",
        "longitude": "-73.977326",
        "icon": "_inc/img/Icon-School@2x.png"
    }, {
        "id": 32,
        "description": "<div class='infobox'>Science Language & Arts International</div>",
        "latitude": "40.686149",
        "longitude": "-73.977405",
        "icon": "_inc/img/Icon-School@2x.png"
    }, {
        "id": 33,
        "description": "<div class='infobox'>Bishop Laughlin Memorial High School</div>",
        "latitude": "40.686843",
        "longitude": "-73.969442",
        "icon": "_inc/img/Icon-School@2x.png"
    }, {
        "id": 34,
        "description": "<div class='infobox'>Hanson Place Elementary</div>",
        "latitude": "40.686731",
        "longitude": "-73.977172",
        "icon": "_inc/img/Icon-School@2x.png"
    }
    ];
    for (var i = 0; i < json.length; i++) {
        var obj = json[i];
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(obj.latitude, obj.longitude),
            map: map,
            icon: obj.icon,
            title: "test"
        });
        var clicker = addClicker(marker, obj.description);
    }
    function addClicker(marker, content) {
        google.maps.event.addListener(marker, 'click', function() {
            if (infowindow) {
                infowindow.close();
            }
            infowindow = new google.maps.InfoWindow({
                content: content
            });
            infowindow.open(map, marker);
        });
    }
    infowindow = new google.maps.InfoWindow();
}